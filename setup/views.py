from django.shortcuts import render, redirect, get_object_or_404
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth import get_user_model, authenticate, login as dj_login, logout as s_logout
from django.contrib.auth import user_logged_in
from django.dispatch.dispatcher import receiver
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from web.models import *
from django.core.mail import EmailMultiAlternatives, message
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from setup.settings import EMAIL_FROM
from django.db.models import Sum
import random
import string
import uuid
import datetime
import json
from django.db.models import Q
from django.http import HttpResponse
from datetime import datetime
from django.views.decorators.http import require_http_methods
from django.views.decorators.http import require_POST
from django.views.decorators.csrf import csrf_exempt
User = get_user_model()
from django.http import HttpResponse
from collections import OrderedDict
from django.core import serializers
from django.http import JsonResponse
from function import *
from payment import *
My_Class = Main()
today = datetime.datetime.now()
now = today.strftime('%Y-%m-%d')



def index(request):
    return render(request, 'home/index.html')

def login(request):
    if request.user.is_authenticated:
        return redirect('/dashboard')
    if request.method == "POST":
        email = request.POST['email']
        password = request.POST['password']

        resp = My_Class.Login(request, email, password)
        return resp
    else:
        return render(request, 'login.html')


def register(request):
    if request.user.is_superuser == True:
        return redirect('/web/home')
    if request.method == "POST":
        role = request.POST['role']
        firstname = request.POST['firstname']
        lastname = request.POST['lastname']
        email = request.POST['email']
        mobile = request.POST['mobile']
        password = request.POST['password']
        if role == "":
            messages.error(request, "Please select Account type")
            return render(request, 'signup.html', {'email': email, 'firstname': firstname, 'lastname': lastname, 'mobile': mobile})
        elif role == 'user':
            resp = My_Class.Register(request, firstname, lastname, mobile, email, password, role="user")
            return resp
        else:
            resp = My_Class.Add_Vendor(request, firstname, lastname, mobile, email, password, role)
            return resp
    else:
        return render(request, 'signup.html')


@login_required(login_url='/login')
def dashboard(request):
    uid = request.user.id
    status = My_Class.check_kyc_status(uid)
    kyc_exist = My_Class.check_kyc_info(uid)
    if request.user.is_superuser == True:
        return redirect('/web/home')
    allBal = My_Class.Wallet_Balance(request)
    ShowLog = My_Class.Show_Log_With_Limit(request)
    today_trans = My_Class.Today_Transaction(tdate=now, uid=uid)[1].count()
    ngn_bal = My_Class.Received(uid, cur='ngn', sid=None)[0].aggregate(Sum('amt_rec'))['amt_rec__sum']
    btc_bal = My_Class.Received(uid, cur='btc', sid=None)[0].aggregate(Sum('amt_rec'))['amt_rec__sum']
    eth_bal = My_Class.Received(uid, cur='eth', sid=None)[0].aggregate(Sum('amt_rec'))['amt_rec__sum']
    tdeposit = My_Class.Received(uid, cur='ngn', sid=0)[0].aggregate(Sum('amt_rec'))['amt_rec__sum']
    return render(request, 'home.html', {'allBal': allBal, 'ShowLog': ShowLog, 'today_trans': today_trans, 
            'ngn_bal': ngn_bal, 'btc_bal': btc_bal, 'eth_bal': eth_bal, 'tdeposit': tdeposit, 'status':status, 'kyc_exist':kyc_exist})


def logout(request):
    resp = My_Class.Logout(request)
    return resp


@login_required(login_url='/login')
def profile(request):
    user_id = request.user.id
    status = My_Class.check_kyc_status(user_id)
    kyc_exist = My_Class.check_kyc_info(user_id)
    if request.user.is_superuser == True:
        return redirect('/web/home')
    return render(request, 'profile.html', {'status':status, 'kyc_exist':kyc_exist})


@login_required(login_url='/login')
def send_money(request):
    user_id = request.user.id
    status = My_Class.check_kyc_status(user_id)
    kyc_exist = My_Class.check_kyc_info(user_id)
    if request.user.is_superuser == True:
        return redirect('/web/home')
    if request.method == "POST":
        rec_email = request.POST['rec_email']
        send_acct = request.POST['send_acct']
        amount = request.POST['amount']
        rec_acct = request.POST['rec_acct']

        resp = My_Class.Send_Money(request, rec_email, send_acct, amount, rec_acct)
        return resp
    else:
        allBal = My_Class.Wallet_Balance(request)
        return render(request, 'send.html', {'allBal': allBal, 'status':status, 'kyc_exist':kyc_exist})


@login_required(login_url='/login')
def transactions(request):
    user_id = request.user.id
    status = My_Class.check_kyc_status(user_id)
    kyc_exist = My_Class.check_kyc_info(user_id)
    show = My_Class.Log(request).order_by('-id')
    return render(request, 'log.html', {'show': show, 'status':status, 'kyc_exist':kyc_exist})


@login_required(login_url='/login')
def deposit(request):
    user_id = request.user.id
    status = My_Class.check_kyc_status(user_id)
    kyc_exist = My_Class.check_kyc_info(user_id)
    if request.user.is_superuser == True:
        return redirect('/web/home')
    if request.method == "POST":
        amount = request.POST['amount']
        confirm = True
        final_amount = (float(amount))
        name = 'monify'
        paygate = My_Class.Payment(name)
        return render(request, 'deposit.html', {'amount': final_amount, 'confirm': confirm, 'paygate':paygate})
    else:
        return render(request, 'deposit.html', {'status':status, 'kyc_exist':kyc_exist})


@login_required(login_url='/login')
def confirm_payment(request):
    if request.user.is_superuser == True:
        return redirect('/web/home')
    if request.method == "GET":
        payref = request.GET.get('payref')
        status = request.GET.get('status')
        paymentStatus = request.GET.get('paymentStatus')
        amount = request.GET.get('amount')
        desc = request.GET.get('desc')
        
        resp = My_Class.Update_Balance(request, payref, status, paymentStatus, amount, desc)
        return resp


@login_required(login_url='/login')
def user_kyc(request):
    user_id = request.user.id
    if request.method == "POST":
        firstname = request.POST['firstname']
        lastname = request.POST['lastname']
        email = request.POST['email']
        address = request.POST['address']
        city = request.POST['city']
        state = request.POST['state']
        zipcode = request.POST['zipcode']
        doc_type = request.POST['doc_type']
        doc_no = request.POST['doc_no']
        file = request.FILES['file']
        My_Class.customer_kyc(user_id, firstname, lastname, email, address, city, state, zipcode, doc_type, doc_no, file)
        return redirect('/user_kyc')
    else:
        status = My_Class.check_kyc_status(user_id)
        kyc_exist = My_Class.check_kyc_info(user_id)
        if status == 2 or status == 4 or kyc_exist == False:
            return render(request, 'kyc.html',{'status':status, 'kyc_exist':kyc_exist})
        else:
            return redirect('/dashboard')


@login_required(login_url='/login')
def settlement(request):
    userid = request.user.id
    if request.method == "POST":
        accno = request.POST['accno']
        sbank = request.POST['bank']
        accname = request.POST['accname']

        if sbank == "":
            messages.error(request, "Please Select Your bank")
            return redirect('/settlement')
        else:
            My_Class.AddSettlementAcct(userid, sbank, accname, accno)
            messages.success(request, "Payout Record Updated Successful")
            return redirect('/settlement')
    else:
        bank = My_Class.showAllBank()
        showbank = My_Class.ShowSettlementAccount(userid)
        return render(request, 'settlement.html', {'bank':bank, 'showbank':showbank})


def verifyBank(request):
    if request.method == "POST":
        bank = request.POST['bank']
        acctno = request.POST['accno']
        r_dict = VerifyAccount(acctno, bank)

        acc = []
        for i in r_dict:
            bnk = r_dict[i]
            acc.append(bnk)
        status = acc[1]
        if status == "success":
            ac_d = acc[3]
            acct_num = ac_d['accountNumber']
            acct_name = ac_d['accountName']
            bnk_code = ac_d['bankCode']
            print(acct_name)

            data = {
                "acctname": acct_name
            }
            # print(data)
            return JsonResponse(data)
        else:
            data = {
                "acctname": status
            }
            # print(data)
            return JsonResponse(data)


@login_required(login_url='/login')
def business(request):
    email = request.user.email
    if request.method == "POST":
        contract = request.POST['contract']
        busname = request.POST['busname']
        bus_email = request.POST['bus_email']
        busno = request.POST['busno']
        bus_address = request.POST['bus_address']
        bus_logo = request.FILES['file']
        resp = My_Class.UpdateVendor(request, contract, busname, bus_email, busno, bus_address, bus_logo)
        return resp
    else:
        allv = My_Class.ShowVendor(email)
        return render(request, 'business.html',{'allv':allv})