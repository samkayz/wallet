from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.core.paginator import Paginator
from django.utils.html import strip_tags
from django.db.models import Sum
from setup.settings import EMAIL_FROM
from django.contrib.auth import get_user_model, authenticate, login as dj_login, logout as s_logout
User = get_user_model()



class Email:


    def Reg_Email(self, subject, email, name, password):
        subject, from_email, to = subject, EMAIL_FROM, email
        html_content = render_to_string('email/registration.html', {'name': name, 'password': password})
        text_content = strip_tags(html_content)
        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
        msg.attach_alternative(html_content, "text/html")
        return msg.send()


    
    def Send_Money(self, email, amount, cur, name, receiver):
        subject, from_email, to = "Money Sent", EMAIL_FROM, email
        html_content = render_to_string('email/send.html', {'amount': amount, 'cur': cur, 'name': name, 'receiver': receiver})
        text_content = strip_tags(html_content)
        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
        msg.attach_alternative(html_content, "text/html")
        return msg.send()

    
    def Rec_Money(self, email, amount, cur, name, bal, sender):
        subject, from_email, to = "Money Recieved", EMAIL_FROM, email
        html_content = render_to_string('email/recieve.html', {'amount': amount, 'cur': cur, 'name': name, 'bal': bal, 'sender': sender})
        text_content = strip_tags(html_content)
        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
        msg.attach_alternative(html_content, "text/html")
        return msg.send()

    
    def NofifyUserKyc(self, firstname, email):
        subject, from_email, to = "KYC Verifcation", EMAIL_FROM, email
        html_content = render_to_string('email/kyc.html', {'firstname': firstname})
        text_content = strip_tags(html_content)
        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
        msg.attach_alternative(html_content, "text/html")
        return msg.send()