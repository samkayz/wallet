import requests
import json
import pygeoip
import geoip2.database
from requests.auth import HTTPBasicAuth
from web.models import PaymentSetup

try:
    moni = PaymentSetup.objects.all().get(name='monify')
except:
    moni = ""


# urls = 'https://sandbox.monnify.com/api/v1'

def Auth():
    response = requests.post(f'{moni.baseurl}/api/v1/auth/login', 
    auth=HTTPBasicAuth(moni.api_key, moni.sec_key))

    response_dict = json.loads(response.text)
    h = []
    for i in response_dict:
        data = response_dict[i]
        h.append(data)
    e = []
    for r in h[3]:
        toke = h[3][r]
        e.append(toke)
    a = "Bearer"+ " " + e[0]
    return a


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        print ("returning FORWARDED_FOR")
        ip = x_forwarded_for.split(',')[-1].strip()
    elif request.META.get('HTTP_X_REAL_IP'):
        print ("returning REAL_IP")
        ip = request.META.get('HTTP_X_REAL_IP')
    else:
        print ("returning REMOTE_ADDR")
        ip = request.META.get('REMOTE_ADDR')
    return ip



def geoLocation(ip):
    reader = geoip2.database.Reader('/Users/ittrade/PycharmProjects/wallet/location/GeoLite2-City.mmdb')
    response = reader.city(ip)
    lcity = response.city.names['en']
    cont = response.country.name[0:]
    conti = response.continent.name[0:]

    data = {'country': cont, 'city': lcity, 'continent':conti}
    return data


def VerifyAccount(acctno, bcode):
    url = f'{moni.baseurl}/api/v1/disbursements/account/validate?accountNumber={acctno}&bankCode={bcode}'

    payload = {}
    headers= {}

    response = requests.request("GET", url, headers=headers, data = payload)

    r_dict = json.loads(response.text)
    return r_dict


def BankTransfer(amount, txnid, desc, bcode, acctno):
    url = f'{moni.baseurl}/api/v1/disbursements/single'

    payload = {
        'amount': amount,
        'reference': txnid,
        'narration': desc,
        'bankCode': bcode,
        'accountNumber': acctno,
        'currency': 'NGN',
        'walletId': moni.mwal_id
        }
    headers = {
        'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, auth=HTTPBasicAuth(moni.api_key, moni.sec_key), headers=headers, data = json.dumps(payload))

    r_dict = json.loads(response.text)
    return r_dict