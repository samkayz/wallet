from django.shortcuts import render
from function import *
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from django.contrib.auth import get_user_model
from web.models import *
from rest_framework import status
from rest_framework.response import Response
import random
import string

# Create your views here.
MyClass = Main()
User = get_user_model()



@api_view(['POST'])
@permission_classes([IsAuthenticated])
def pay(request):
    N = 9
    ref = ''.join(random.choices(string.digits, k=N))
    cust_email = request.user.email
    """
    docstring
    """
    contract = request.data.get('contract')
    cur = request.data.get('currency')
    desc = request.data.get('desc')
    amount = request.data.get('amount')

    amt = float(amount)

    ## Check Balance of Login User
    if Vendor_Contract.objects.filter(contract=contract).exists():
        vend_email = MyClass.check_contract_code(contract).user_email
        if vend_email == cust_email:
            data = {
                "code": status.HTTP_401_UNAUTHORIZED,
                "status": "fail",
                "message": "Please you can't pay yourself"
            }
            return Response(data=data, status=status.HTTP_401_UNAUTHORIZED)
        
        elif MyClass.check_balance(cust_email, cur) < amt:
            data = {
                "code": status.HTTP_402_PAYMENT_REQUIRED,
                "status": "fail",
                "message": "Insufficient Balance"
            }
            return Response(data=data, status=status.HTTP_402_PAYMENT_REQUIRED)
        else:
            cust_bal = float(MyClass.check_balance(cust_email, cur))
            vend_bal = float(MyClass.check_balance(vend_email, cur))

            ### Update Customer Balence.
            new_cust_bal = cust_bal - amt
            MyClass.update_api_balance(cust_email, cur, new_cust_bal)


            ### Update Vendor Balance

            new_vend_bal = vend_bal + amt
            MyClass.update_api_balance(vend_email, cur, new_vend_bal)

            MyClass.create_transaction_log(vend_email, cust_email, cur, amt, cur, amt, ref)

            data = {
                "code": status.HTTP_200_OK,
                "status": "successful",
                "txnRef": ref,
                "message": "Transaction Successful"
            }
            return Response(data=data, status=status.HTTP_200_OK)

    else:
        data = {
            "code": status.HTTP_400_BAD_REQUEST,
            "status": "fail",
            "message": "Invalid contract code"
        }
        return Response(data=data, status=status.HTTP_400_BAD_REQUEST)



@api_view(['GET'])
@permission_classes([])
def verify_trans(request, txnRef):
    try:
        txn_info = Transaction_Log.objects.all().get(ref=txnRef)

        data = {
            "code": status.HTTP_200_OK,
            "txnRef": txn_info.ref,
            "sender": txn_info.snd,
            "reciever": txn_info.rec,
            "amountSend": txn_info.amt_snd,
            "sendCurrency": txn_info.scur,
            "amountRecieved": txn_info.amt_rec,
            "recievedCurrency": txn_info.rcur,
            "txnDate": txn_info.date
        }
        return Response(data=data, status=status.HTTP_200_OK)
    
    except:
        data = {
            "code": status.HTTP_404_NOT_FOUND,
            "message": "transaction detail not found"
        }
        return Response(data=data, status=status.HTTP_404_NOT_FOUND)
