from web.models import *
from django.db.models import Q
from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth import get_user_model, authenticate, login as dj_login, logout as s_logout
from django.contrib.auth import user_logged_in
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
import json
User = get_user_model()
import os
import random
import string
import uuid
import datetime
import time
from django.core.mail import EmailMultiAlternatives, send_mail
from django.template.loader import render_to_string
from django.core.paginator import Paginator
from django.utils.html import strip_tags
from django.db.models import Sum
from setup.settings import EMAIL_FROM

from SendEmail import *

send = Email()



def check_kyc_exist(user_id):
    check = Kyc.objects.filter(user_id=user_id).exists()
    return check
__name__ == "__main__"
class Main:



    ### Login Function ####
    def Login(self, request, email, password):
        user = authenticate(email=email, password=password)
        if User.objects.filter(email=email, is_superuser=True).exists():
            messages.error(request, 'Invalid Credentials')
            return render(request, 'login.html', {'email': email})
        elif user is not None:
            dj_login(request, user)
            # request.session.set_expiry(1200)
            response = redirect('/dashboard')
            return response
        else:
            messages.error(request, 'Invalid Credentials')
            return render(request, 'login.html', {'email': email})

    #### Logout Function
    def Logout(self, request):
        s_logout(request)
        messages.success(request, "Logout Successfully")
        resp = redirect('/login')
        return resp

    
    ##### Regististration #######
    def Register(self, request, firstname, lastname, mobile, email, password, role):
        if User.objects.filter(email=email).exists():
            messages.error(request, 'Email Used')
            return render(request, 'signup.html', {'email': email, 'firstname': firstname, 'lastname': lastname, 'mobile': mobile})
        elif User.objects.filter(mobile=mobile).exists():
            messages.error(request, 'Mobile Number Used')
            return render(request, 'signup.html', {'email': email, 'firstname': firstname, 'lastname': lastname, 'mobile': mobile})
        else:
            user = User.objects.create_user(firstname=firstname, lastname=lastname, mobile=mobile, email=email, password=password, role=role)
            wal = Wallet(user_email=email)
            wal.save()
            user.save()

            ### Send Confirmation Email to the User
            send.Reg_Email(subject=f'Welcome {firstname}', email=email, name=firstname, password=password)

            user = authenticate(email=email, password=password)
            dj_login(request, user)
            # request.session.set_expiry(1200)
            return redirect('dashboard')
    

    def Add_Vendor(self, request, firstname, lastname, mobile, email, password, role):

        P = 9
        contract = ''.join(random.choices(string.digits, k=P))
        
        if User.objects.filter(email=email).exists():
            messages.error(request, 'Email Used')
            return render(request, 'signup.html', {'email': email, 'firstname': firstname, 'lastname': lastname, 'mobile': mobile})
        elif User.objects.filter(mobile=mobile).exists():
            messages.error(request, 'Mobile Number Used')
            return render(request, 'signup.html', {'email': email, 'firstname': firstname, 'lastname': lastname, 'mobile': mobile})
        else:
            user = User.objects.create_user(firstname=firstname, lastname=lastname, mobile=mobile, email=email, password=password, role=role)
            wal = Wallet(user_email=email)
            vend = Vendor_Contract(user_email=email, contract=contract)
            vend.save()
            wal.save()
            user.save()

            ### Send Confirmation Email to the User
            send.Reg_Email(subject=f'Welcome {firstname}', email=email, name=firstname, password=password)
            user = authenticate(email=email, password=password)
            dj_login(request, user)
            # request.session.set_expiry(1200)
            return redirect('dashboard')
    

    def Wallet_Balance(self, request):
        user_email = request.user.email

        allBal = Wallet.objects.all().get(user_email=user_email)
        return allBal
    

    def Send_Money(self, request, rec_email, send_acct, amount, rec_acct):
        sender_email = request.user.email
        N = 9
        ref = ''.join(random.choices(string.digits, k=N))
        if User.objects.filter(email=rec_email).exists():
            sbal = Wallet.objects.values(send_acct).get(user_email=sender_email)[send_acct]
            rbal = Wallet.objects.values(rec_acct).get(user_email=rec_email)[rec_acct]

            sid = User.objects.all().get(email=sender_email)
            rid = User.objects.all().get(email=rec_email)

            amt = float(amount)
            if amt > sbal:
                messages.error(request, 'Insufficient Balance')
                return redirect('/send_money')
            elif send_acct == "ngn" and amt < 100:
                messages.error(request, 'Please you cant send less than 100 Naira')
                return redirect('/send_money')
            elif rec_email == sender_email:
                messages.error(request, "You can't send money to yourself")
                return redirect('/send_money')
            else:
                new_snd_bal = sbal - amt
                new_rec_bal = amt + rbal

                fullname = rid.firstname + ' ' + rid.lastname
                sfull = sid.firstname + ' '+ sid.lastname
                if send_acct == "ngn" and rec_acct == "ngn":

                    
                    Wallet.objects.filter(user_email=sender_email).update(ngn=new_snd_bal)
                    Wallet.objects.filter(user_email=rec_email).update(ngn=new_rec_bal)

                    log = Transaction_Log(sid=sid.id, rid=rid.id, ref=ref, snd=sid.firstname + ' '+ sid.lastname, rec=rid.firstname + ' ' + rid.lastname, scur=send_acct, amt_snd=amt, rcur=rec_acct, amt_rec=amt)
                    log.save()

                    send.Send_Money(sender_email, amount, send_acct, sid.firstname, fullname)
                    send.Rec_Money(rec_email, amt, rec_acct, rid.firstname, rbal, sfull)

                    messages.success(request, 'Transaction Successfull')
                    return redirect('/send_money')

                elif send_acct == "ngn" and rec_acct == "btc":
                    Wallet.objects.filter(user_email=sender_email).update(ngn=new_snd_bal)
                    Wallet.objects.filter(user_email=rec_email).update(btc=new_rec_bal)

                    log = Transaction_Log(sid=sid.id, rid=rid.id, ref=ref, snd=sid.firstname + ' '+ sid.lastname, rec=rid.firstname + ' ' + rid.lastname, scur=send_acct, amt_snd=amt, rcur=rec_acct, amt_rec=amt)
                    log.save()

                    send.Send_Money(sender_email, amount, send_acct, sid.firstname, fullname)
                    send.Rec_Money(rec_email, amt, rec_acct, rid.firstname, rbal, sfull)

                    messages.success(request, 'Transaction Successfull')
                    return redirect('/send_money')

                elif send_acct == "ngn" and rec_acct == "eth":
                    Wallet.objects.filter(user_email=sender_email).update(ngn=new_snd_bal)
                    Wallet.objects.filter(user_email=rec_email).update(eth=new_rec_bal)

                    log = Transaction_Log(sid=sid.id, rid=rid.id, ref=ref, snd=sid.firstname + ' '+ sid.lastname, rec=rid.firstname + ' ' + rid.lastname, scur=send_acct, amt_snd=amt, rcur=rec_acct, amt_rec=amt)
                    log.save()

                    send.Send_Money(sender_email, amount, send_acct, sid.firstname, fullname)
                    send.Rec_Money(rec_email, amt, rec_acct, rid.firstname, rbal, sfull)

                    messages.success(request, 'Transaction Successfull')
                    return redirect('/send_money')

                elif send_acct == "btc" and rec_acct == "ngn":
                    Wallet.objects.filter(user_email=sender_email).update(btc=new_snd_bal)
                    Wallet.objects.filter(user_email=rec_email).update(ngn=new_rec_bal)

                    log = Transaction_Log(sid=sid.id, rid=rid.id, ref=ref, snd=sid.firstname + ' '+ sid.lastname, rec=rid.firstname + ' ' + rid.lastname, scur=send_acct, amt_snd=amt, rcur=rec_acct, amt_rec=amt)
                    log.save()

                    send.Send_Money(sender_email, amount, send_acct, sid.firstname, fullname)
                    send.Rec_Money(rec_email, amt, rec_acct, rid.firstname, rbal, sfull)

                    messages.success(request, 'Transaction Successfull')
                    return redirect('/send_money')

                elif send_acct == "btc" and rec_acct == "btc":
                    Wallet.objects.filter(user_email=sender_email).update(btc=new_snd_bal)
                    Wallet.objects.filter(user_email=rec_email).update(btc=new_rec_bal)

                    log = Transaction_Log(sid=sid.id, rid=rid.id, ref=ref, snd=sid.firstname + ' '+ sid.lastname, rec=rid.firstname + ' ' + rid.lastname, scur=send_acct, amt_snd=amt, rcur=rec_acct, amt_rec=amt)
                    log.save()

                    send.Send_Money(sender_email, amount, send_acct, sid.firstname, fullname)
                    send.Rec_Money(rec_email, amt, rec_acct, rid.firstname, rbal, sfull)

                    messages.success(request, 'Transaction Successfull')
                    return redirect('/send_money')

                elif send_acct == "btc" and rec_acct == "eth":
                    Wallet.objects.filter(user_email=sender_email).update(btc=new_snd_bal)
                    Wallet.objects.filter(user_email=rec_email).update(eth=new_rec_bal)

                    log = Transaction_Log(sid=sid.id, rid=rid.id, ref=ref, snd=sid.firstname + ' '+ sid.lastname, rec=rid.firstname + ' ' + rid.lastname, scur=send_acct, amt_snd=amt, rcur=rec_acct, amt_rec=amt)
                    log.save()

                    send.Send_Money(sender_email, amount, send_acct, sid.firstname, fullname)
                    send.Rec_Money(rec_email, amt, rec_acct, rid.firstname, rbal, sfull)

                    messages.success(request, 'Transaction Successfull')
                    return redirect('/send_money')

                elif send_acct == "eth" and rec_acct == "ngn":
                    Wallet.objects.filter(user_email=sender_email).update(eth=new_snd_bal)
                    Wallet.objects.filter(user_email=rec_email).update(ngn=new_rec_bal)

                    log = Transaction_Log(sid=sid.id, rid=rid.id, ref=ref, snd=sid.firstname + ' '+ sid.lastname, rec=rid.firstname + ' ' + rid.lastname, scur=send_acct, amt_snd=amt, rcur=rec_acct, amt_rec=amt)
                    log.save()

                    send.Send_Money(sender_email, amount, send_acct, sid.firstname, fullname)
                    send.Rec_Money(rec_email, amt, rec_acct, rid.firstname, rbal, sfull)

                    messages.success(request, 'Transaction Successfull')
                    return redirect('/send_money')

                elif send_acct == "eth" and rec_acct == "btc":
                    Wallet.objects.filter(user_email=sender_email).update(eth=new_snd_bal)
                    Wallet.objects.filter(user_email=rec_email).update(btc=new_rec_bal)

                    log = Transaction_Log(sid=sid.id, rid=rid.id, ref=ref, snd=sid.firstname + ' '+ sid.lastname, rec=rid.firstname + ' ' + rid.lastname, scur=send_acct, amt_snd=amt, rcur=rec_acct, amt_rec=amt)
                    log.save()

                    send.Send_Money(sender_email, amount, send_acct, sid.firstname, fullname)
                    send.Rec_Money(rec_email, amt, rec_acct, rid.firstname, rbal, sfull)

                    messages.success(request, 'Transaction Successfull')
                    return redirect('/send_money')

                else:
                    Wallet.objects.filter(user_email=sender_email).update(eth=new_snd_bal)
                    Wallet.objects.filter(user_email=rec_email).update(eth=new_rec_bal)

                    log = Transaction_Log(sid=sid.id, rid=rid.id, ref=ref, snd=sid.firstname + ' '+ sid.lastname, rec=rid.firstname + ' ' + rid.lastname, scur=send_acct, amt_snd=amt, rcur=rec_acct, amt_rec=amt)
                    log.save()

                    send.Send_Money(sender_email, amount, send_acct, sid.firstname, fullname)
                    send.Rec_Money(rec_email, amt, rec_acct, rid.firstname, rbal, sfull)

                    messages.success(request, 'Transaction Successfull')
                    return redirect('/send_money')
            return redirect('/send_money')
        else:
             messages.error(request, 'User not found')
             return redirect('/send_money')

    
    def Log(self, request):
        user_id = request.user.id
        show = Transaction_Log.objects.filter(Q(sid=user_id) | Q(rid=user_id))
        return show

    def Update_Balance(self, request, payref, status, paymentStatus, amount, desc):
        user_email = request.user.email
        if status == "SUCCESS" and paymentStatus == "PAID":
            rid = User.objects.all().get(email=user_email)
            bal = Wallet.objects.values('ngn').get(user_email=user_email)['ngn']
            amt = float(amount)

            newbal = bal + amt
            Wallet.objects.filter(user_email=user_email).update(ngn=newbal)

            log = Transaction_Log(sid=0, rid=rid.id, snd=desc, ref=payref, rec=rid.firstname + ' ' + rid.lastname, scur='ngn', amt_snd=amt, rcur='ngn', amt_rec=amt)
            log.save()
            send.Rec_Money(user_email, amt, cur='ngn', name=rid.firstname, bal=newbal, sender='Wallet Load')
            messages.success(request, 'Transaction Successfull')
            return redirect('/deposit')
        else:
            messages.error(request, 'Transaction Fail')
            return redirect('/deposit')
    
    def Show_Log_With_Limit(self, request):
        user_id = request.user.id
        show = Transaction_Log.objects.filter(Q(sid=user_id) | Q(rid=user_id)).order_by('-id')[:5]
        return show


    def AdminLogin(self, request, email, password):
        user = authenticate(email=email, password=password)
        if User.objects.filter(email=email, is_superuser=False).exists():
            messages.error(request, 'Invalid Credentials')
            return render(request, 'admin/login.html', {'email': email})
        elif user is not None:
            dj_login(request, user)
            response = redirect('/web/home')
            return response
        else:
            messages.error(request, 'Invalid Credentials')
            return render(request, 'admin/login.html', {'email': email})
    

    def AdminLogout(self, request):
        email = request.user.email
        s_logout(request)
        messages.success(request, "Logout Successfully")
        resp = redirect('/web/')
        return resp

    
    def All_History(self):
        show = Transaction_Log.objects.filter()
        return show

    def Today_Transaction(self, tdate, uid):
        today_trans = Transaction_Log.objects.filter(date__startswith=tdate)
        user_today_trans = Transaction_Log.objects.filter(Q(date__startswith=tdate, rid__startswith=uid) | Q(date__startswith=tdate, sid__startswith=uid))
        tsum = Transaction_Log.objects.filter(Q(rid__startswith=uid) | Q(sid__startswith=uid))
        return today_trans, user_today_trans, tsum

    def All_History_With_List(self):
        show_log = Transaction_Log.objects.filter().order_by('-id')[:5]
        return show_log

    
    def All_User(self, role):
        show = Wallet.objects.filter()
        shows = User.objects.filter()
        count = User.objects.filter(role=role)
        return shows, show, count

    def Received(self, uid, cur, sid):
        total_trans = Transaction_Log.objects.filter(Q(rid=uid, rcur=cur))
        total_dep = Transaction_Log.objects.filter(Q(rid=uid, rcur=cur, sid=sid))
        return total_trans, total_dep


    def check_contract_code(self, contract):
        vend_email = Vendor_Contract.objects.all().get(contract=contract)
        return vend_email

    
    def check_balance(self, user_email, cur):
        bal = Wallet.objects.values(cur).get(user_email=user_email)[cur]
        return bal

    def update_api_balance(self, user_email, cur, new_bal):
        if cur == "ngn":
            return Wallet.objects.filter(user_email=user_email).update(ngn=new_bal)
        elif cur == "btc":
            return Wallet.objects.filter(user_email=user_email).update(btc=new_bal)
        else:
            return Wallet.objects.filter(user_email=user_email).update(eth=new_bal)
    
    def create_transaction_log(self, rec_email, snd_email, snd_cur, amt_snd, rec_cur, amt_rec, ref):
        all_snd_info = User.objects.all().get(email=snd_email)
        all_rec_info = User.objects.all().get(email=rec_email)

        rbal = Wallet.objects.values(rec_cur).get(user_email=rec_email)[rec_cur]
        log = Transaction_Log(sid=all_snd_info.id, rid=all_rec_info.id, ref=ref, snd=all_snd_info.firstname + ' '+ all_snd_info.lastname, rec=all_rec_info.firstname + ' ' + all_rec_info.lastname, scur=snd_cur, amt_snd=amt_snd, rcur=rec_cur, amt_rec=amt_rec)
        log.save()
        send.Send_Money(snd_email, amt_snd, snd_cur, all_snd_info.firstname, all_rec_info.firstname + ' ' + all_rec_info.lastname)
        send.Rec_Money(rec_email, amt_rec, snd_cur, all_rec_info.firstname, rbal, all_snd_info.firstname + ' '+ all_snd_info.lastname)
        pass


    def customer_kyc(self, user_id, firstname, lastname, email, address, city, state, zipcode, doc_type, doc_no, file):
        if check_kyc_exist(user_id) == True:
            Kyc.objects.filter(user_id=user_id).update(firstname=firstname, lastname=lastname, user_email=email, 
            address=address, city=city, state=state, zipcode=zipcode, doc_type=doc_type, doc_no=doc_no, doc_file=file)
        else:
            new_kyc = Kyc(user_id=user_id, firstname=firstname, lastname=lastname, user_email=email, address=address, 
            city=city, state=state, zipcode=zipcode, doc_type=doc_type, doc_no=doc_no, doc_file=file)
            new_kyc.save()
        pass


    def check_kyc_status(self, user_id):
        try:
            status = Kyc.objects.values('status').get(user_id=user_id)['status']
            return status
        except:
            status = 3
            return status

    def check_kyc_info(self, user_id):
        check = check_kyc_exist(user_id)
        return check

    
    def ShowKyc(self):
        show = Kyc.objects.filter().order_by('-id')
        return show

    
    def showAllBank(self):
        bank = Banks.objects.filter()
        return bank

    
    def showPayment(self, name):
        try:
            res = PaymentSetup.objects.all().get(name=name)
            return res
        except:
            res = ''
            return res

    
    def CreatePaymentSetup(self, name, baseurl, api_key, sec_key, mcode, mwal_id):
        if PaymentSetup.objects.filter(name=name).exists():
            upay = PaymentSetup.objects.filter(name=name)
            upay.update(baseurl=baseurl, api_key=api_key, sec_key=sec_key, mcode=mcode, mwal_id=mwal_id)
            pass

        else:
            pay = PaymentSetup(name=name, baseurl=baseurl, api_key=api_key, sec_key=sec_key, mcode=mcode, mwal_id=mwal_id)
            pay.save()
            pass

    
    def ShowSettlementAccount(self, userid):
        try:
            setAccount = PayoutAccount.objects.all().get(userid=userid)
            return setAccount
        except:
            setAccount = ''
            return setAccount

    
    def AddSettlementAcct(self, userid, bank_code, acct_name, acct_no):
        bank_name = Banks.objects.values('name').get(code=bank_code)['name']
        if PayoutAccount.objects.filter(userid=userid).exists():
            updateSetAcct = PayoutAccount.objects.filter(userid=userid)
            updateSetAcct.update(bank_name=bank_name, bank_code=bank_code, acct_name=acct_name, acct_no=acct_no)
            pass
        else:
            createSetAcct = PayoutAccount(userid=userid, bank_name=bank_name, bank_code=bank_code, acct_name=acct_name, acct_no=acct_no)
            createSetAcct.save()
            pass
    

    def ShowVendor(self, email):
        try:
            allv = Vendor_Contract.objects.all().get(user_email=email)
        except:
            allv = ''
        return allv
    
    def ShowVendors(self, contract):
        try:
            allv = Vendor_Contract.objects.all().get(contract=contract)
        except:
            allv = ''
        return allv

    
    def UpdateVendor(self, request, contract, busname, bus_email, busno, bus_address, bus_logo):
        instance = Vendor_Contract.objects.filter(contract=contract)
        instance.update(busname=busname, bus_email=bus_email, busno=busno, bus_address=bus_address)
        logo = Vendor_Contract.objects.get(contract=contract)
        logo.bus_logo = bus_logo
        logo.save()
        messages.success(request, "Business Information Updated")
        return redirect('/business')
    
    def CheckBalance(self, amount, user_email):
        amt = float(amount)
        pmat = Wallet.objects.values('ngn').get(user_email=user_email)['ngn']
        if amt > pmat:
            return True
        else:
            return False

    def DoVendorPayment(self, ref, desc, s_email, r_email, amount):
        
        sendBal = Wallet.objects.values('ngn').get(user_email=s_email)['ngn']
        vendorBal = Wallet.objects.values('ngn').get(user_email=r_email)['ngn']
        amt = float(amount)
        sid = User.objects.all().get(email=s_email)
        rid = User.objects.all().get(email=r_email)
        #### Sender Wallet Update ####
        newBal = sendBal - amt
        Wallet.objects.filter(user_email=s_email).update(ngn=newBal)

        #### Vendor Wallet Update ####
        newVBal = vendorBal + amt
        Wallet.objects.filter(user_email=r_email).update(ngn=newVBal)

        log = Transaction_Log(sid=sid.id, rid=rid.id, desc=desc, ref=ref, snd=sid.firstname + ' '+ sid.lastname, rec=rid.firstname + ' ' + rid.lastname, scur='ngn', amt_snd=amt, rcur='ngn', amt_rec=amt)
        log.save()
        pass


    def Payment(self, name):
        try:
            paygate = PaymentSetup.objects.all().get(name=name)
        except:
            paygate = ''
        return paygate
