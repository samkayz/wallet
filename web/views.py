from django.shortcuts import render, redirect, get_object_or_404
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth import get_user_model, authenticate, login as dj_login, logout as s_logout
from django.contrib.auth import user_logged_in
from django.dispatch.dispatcher import receiver
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib import messages
from web.models import *
from django.db.models import Sum
import random
import string
import uuid
import datetime
import json
from django.db.models import Q
from datetime import datetime
from django.views.decorators.http import require_http_methods
from django.views.decorators.http import require_POST
from django.views.decorators.csrf import csrf_exempt
User = get_user_model()
from django.http import HttpResponse
from collections import OrderedDict
from django.core import serializers
from django.http import JsonResponse
from SendEmail import *
from function import *
My_Class = Main()
newEmail = Email()
today = datetime.datetime.now()
now = today.strftime('%Y-%m-%d')
from fusioncharts import FusionCharts
from chart import *
newChart = sChart()



# @permission_required('is_superuser', login_url='/console/')
def login(request):
    if request.user.is_authenticated and request.user.is_superuser == True:
        return redirect('/web/home')
    if request.method == "POST":
        email = request.POST['email']
        password = request.POST['password']

        resp = My_Class.AdminLogin(request, email, password)
        return resp
    else:
        return render(request, 'admin/login.html')


@permission_required('is_superuser', login_url='/web/')
def home(request):
    if request.user.is_superuser == False:
        return redirect('/dashboard')
    show_log = My_Class.All_History_With_List()
    resp = My_Class.All_User(role='user')
    count_user = resp[2].count()
    count_vendor = My_Class.All_User(role='vendor')[2].count()
    total_trans = My_Class.All_History().count()
    today_trans = My_Class.Today_Transaction(tdate=now, uid='')[0].count()
    
    column2D = newChart.AllUserChart()
    trans = newChart.CurChart()
    itrans = newChart.InwardChart()
    return render(request, 'admin/home.html', {'show_log': show_log, 'count_user': count_user, 
        'count_vendor': count_vendor, 'total_trans': total_trans, 'today_trans': today_trans, 
        'output': column2D.render(), 'trans': trans.render(), 'itrans': itrans.render()})


def logout(request):
    resp = My_Class.AdminLogout(request)
    return resp


@permission_required('is_superuser', login_url='/web/')
def history(request):
    if request.user.is_superuser == False:
        return redirect('/dashboard')
    show = My_Class.All_History()
    return render(request, 'admin/log.html', {'show': show})


@permission_required('is_superuser', login_url='/web/')
def all_user(request):
    if request.user.is_superuser == False:
        return redirect('/dashboard')
    resp = My_Class.All_User(role=None)
    wallet = resp[1]
    user = resp[0]
    return render(request, 'admin/user.html', {'show_user': user, 'wallet': wallet})


@permission_required('is_superuser', login_url='/web/')
def add_vendor(request):
    if request.user.is_superuser == False:
        return redirect('/dashboard')
    if request.method == "POST":
        firstname = request.POST['firstname']
        lastname = request.POST['lastname']
        email = request.POST['email']
        mobile = request.POST['mobile']
        
        resp = My_Class.Add_Vendor(request, firstname, lastname, mobile, email)
        return resp
    else:
        return render(request, 'admin/addvendor.html')



@permission_required('is_superuser', login_url='/web/')
def all_vendor(request):
    if request.user.is_superuser == False:
        return redirect('/dashboard')
    resp = My_Class.All_User(role=None)
    wallet = resp[1]
    user = resp[0]
    return render(request, 'admin/vendor.html', {'show_user': user, 'wallet': wallet})


@permission_required('is_superuser', login_url='/web/')
def profile(request):
    if request.user.is_superuser == False:
        return redirect('/dashboard')
    return render(request, 'admin/profile.html')


@permission_required('is_superuser', login_url='/web/')
def userKyc(request):
    show = My_Class.ShowKyc()
    return render(request, 'admin/kyc.html', {'show':show})


@permission_required('is_superuser', login_url='/web/')
def approvekyc(request, user_id):
    kycinfo = Kyc.objects.all(). get(user_id=user_id)
    kinstance = Kyc.objects.filter(user_id=user_id)
    kinstance.update(status='1')
    uinstance = User.objects.filter(id=user_id)
    uinstance.update(kyc=True)
    newEmail.NofifyUserKyc(kycinfo.firstname, kycinfo.user_email)
    return redirect('/web/userKyc')


@permission_required('is_superuser', login_url='/web/')
def vendorlog(request, id):
    show = Transaction_Log.objects.filter(Q(rid=id) | Q(sid=id)).order_by('-id')
    return render(request, 'admin/vendorlog.html', {'show':show})


@permission_required('is_superuser', login_url='/web/')
def monify(request):
    if request.method == "POST":
        name = request.POST['name']
        baseurl = request.POST['baseurl']
        apikey = request.POST['apikey']
        skey = request.POST['skey']
        contract = request.POST['contract']
        walletid = request.POST['walletid']

        My_Class.CreatePaymentSetup(name, baseurl, apikey, skey, contract, walletid)
        messages.success(request, "Action Successfull")
        return redirect('/web/monify')
    else:
        res = My_Class.showPayment(name='monify')
        return render(request, 'admin/monify.html', {'res': res})
