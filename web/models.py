from django.db import models
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.models import AbstractBaseUser, UserManager, BaseUserManager
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.contrib.sessions.models import Session

fs = FileSystemStorage(location='/Users/apple/Documents/Project/wallet/media')



class UserManager(BaseUserManager):
    """Define a model manager for User model with no username field."""

    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """Create and save a User with the given email and password."""
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        """Create and save a regular User with the given email and password."""
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """Create and save a SuperUser with the given email and password."""
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)

    
class User(AbstractBaseUser, PermissionsMixin):
    username = None
    email = models.EmailField(_('email address'), unique=True)
    firstname = models.CharField("firstname", max_length=255, blank=True)
    lastname = models.CharField("lastname", max_length=255, blank=True)
    mobile = models.CharField(verbose_name='mobile', max_length=30)
    kyc = models.BooleanField(default=False, null=True)
    is_staff = models.BooleanField(verbose_name='is_staff', default=True)
    date_joined = models.DateTimeField(verbose_name='date_joined', auto_now_add=True)
    is_active = models.BooleanField(verbose_name='is_active', default=True)
    role = models.CharField(verbose_name='role', max_length=255)

    objects =  UserManager()

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    class Meta:
        db_table = 'user'

    def get_full_name(self):
        return self.name

    def get_short_name(self):
        return self.name


class UserSession(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    session = models.OneToOneField(Session, on_delete=models.CASCADE)

    class Meta:
        db_table = 'user_session'



class Wallet(models.Model):
    user_email = models.EmailField()
    ngn = models.FloatField(default=0)
    btc = models.FloatField(default=0)
    eth = models.FloatField(default=0)

    class Meta:
        db_table = 'wallet'



class Transaction_Log(models.Model):
    sid = models.BigIntegerField()
    rid = models.BigIntegerField()
    ref = models.TextField(null=True, blank=True)
    scur = models.TextField()
    snd = models.TextField()
    rec = models.TextField()
    amt_snd = models.FloatField()
    rcur = models.TextField()
    amt_rec = models.FloatField()
    desc = models.TextField(null=True, blank=True)
    date = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'transaction_log'


class Vendor_Contract(models.Model):
    user_email = models.EmailField()
    contract = models.TextField(null=True, blank=True)
    busname = models.TextField(null=True, blank=True)
    bus_email = models.EmailField(null=True, blank=True)
    busno = models.TextField(null=True, blank=True)
    bus_address = models.TextField(null=True, blank=True)
    bus_logo = models.FileField(storage=fs, null=True)
    callback = models.URLField(null=True, blank=True)
    charges = models.TextField(null=True, blank=True)

    class Meta:
        db_table = 'vendor_contract'

class Banks(models.Model):
    name = models.CharField(max_length=255)
    code = models.CharField(max_length=20)

    class Meta:
        db_table = 'banks'


class PayoutAccount(models.Model):
    userid = models.CharField(max_length=255)
    bank_name = models.CharField(max_length=255)
    bank_code = models.CharField(max_length=20)
    acct_name = models.CharField(max_length=255)
    acct_no = models.CharField(max_length=255)

    class Meta:
        db_table = 'payout_account'


class Kyc(models.Model):
    user_id = models.BigIntegerField()
    user_email = models.EmailField(null=True, blank=True)
    firstname = models.TextField(null=True, blank=True)
    lastname = models.TextField(null=True, blank=True)
    address = models.TextField(null=True, blank=True)
    city = models.TextField(null=True, blank=True)
    state = models.TextField(null=True, blank=True)
    zipcode = models.TextField(null=True, blank=True)
    doc_type = models.TextField(null=True, blank=True)
    doc_no = models.TextField(null=True, blank=True)
    doc_file = models.FileField(storage=fs, null=True)
    status = models.IntegerField(default=0)

    class Meta:
        db_table = 'kyc'


class PaymentSetup(models.Model):
    name = models.TextField(blank=True, null=True)
    baseurl = models.URLField()
    api_key = models.TextField(blank=True, null=True)
    sec_key = models.TextField(blank=True, null=True)
    mcode = models.TextField(blank=True, null=True)
    mwal_id = models.TextField(blank=True, null=True)

    class Meta:
        db_table = 'payment_setup'