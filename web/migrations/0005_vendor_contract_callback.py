# Generated by Django 3.1.5 on 2021-02-11 19:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0004_auto_20210209_2019'),
    ]

    operations = [
        migrations.AddField(
            model_name='vendor_contract',
            name='callback',
            field=models.URLField(blank=True, null=True),
        ),
    ]
