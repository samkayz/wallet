from django.urls import path
from . import views
from django.contrib.auth import views as auth_views


urlpatterns = [
    path('', views.login, name='login'),
    path('home', views.home, name='home'),
    path('logout', views.logout, name='logout'),
    path('history', views.history, name='history'),
    path('all_user', views.all_user, name='all_user'),
    path('add_vendor', views.add_vendor, name='add_vendor'),
    path('all_vendor', views.all_vendor, name='all_vendor'),
    path('profile', views.profile, name='profile'),
    path('userKyc', views.userKyc, name='userKyc'),
    path('approvekyc/<user_id>', views.approvekyc, name='approvekyc'),
    path('vendorlog/<id>', views.vendorlog, name='vendorlog'),
    path('monify', views.monify, name='monify'),

    path('password_change/done/', auth_views.PasswordChangeDoneView.as_view(
        template_name='login.html'), name='password_change_done'),

    path('password_change/', auth_views.PasswordChangeView.as_view(template_name='password.html'),
         name='password'),

    path('password_reset_done/', auth_views.PasswordResetCompleteView.as_view(
        template_name='admin/registration/password_reset_done.html'), name='password_reset_done'),

    path('reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('password_reset/', auth_views.PasswordResetView.as_view(template_name='admin/registration/password_reset_form.html'), 
        name='password_reset'),


    path('reset/done/', auth_views.PasswordResetCompleteView.as_view(
        template_name='registration/password_reset_complete.html'), name='password_reset_complete'),
]
