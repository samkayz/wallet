from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import get_user_model, authenticate, login as dj_login, logout as s_logout
from django.contrib import messages
from django.urls import reverse
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.http import JsonResponse
from django.http import HttpResponseRedirect
from web.models import *
from setup.settings import EMAIL_FROM
import uuid
from function import *
from SendEmail import *

semail = Email()

funct = Main()
UserModel = get_user_model()


@csrf_exempt
def pay(request):
    if request.method == 'POST':
        desc = request.POST['desc']
        amount = request.POST['amount']
        contract = request.POST['contract']
        sucess_url = request.POST['sucess_url']
        fail_url = request.POST['fail_url']
        allv = funct.ShowVendors(contract)
        context = {'desc': desc, 'amount': amount, 'contract': contract,'sucess_url': sucess_url, 'fail_url':fail_url, 'logo': allv.bus_logo, 'busname': allv.busname}
        return render(request, 'pay/pay.html', context)


def initiate(request):
    N = 9
    ref = ''.join(random.choices(string.digits, k=N))
    if request.method == 'POST':
        desc = request.POST['desc']
        amount = request.POST['amount']
        contract = request.POST['contract']
        email = request.POST['email']
        password = request.POST['password']
        sucess_url = request.POST['sucess_url']
        fail_url = request.POST['fail_url']
        user = authenticate(email=email, password=password)
        if user is not None:
            dj_login(request, user)
            allv = funct.ShowVendors(contract)
            check = funct.CheckBalance(amount, email)
            if check == True:
                status = 400
                msg = "Insufficient Fund"
                s_logout(request)
                return render(request, 'pay/resp.html', {'status': status,
                                                     'callback': fail_url,
                                                     'msg': msg})
            elif email == allv.user_email:
                status = 400
                msg = "You Can't Send Money to Yourself!!"
                s_logout(request)
                return render(request, 'pay/resp.html', {'status': status,
                                                     'callback': fail_url,
                                                     'msg': msg})
            else:
                funct.DoVendorPayment(ref, desc, email, allv.user_email, amount)
                s_logout(request)
                #Payee Email
                status = 200
                allsend = User.objects.all().get(email=email)
                allrec = User.objects.all().get(email=allv.user_email)
                ## Notify Payer ###
                cur = 'ngn'
                semail.Send_Money(allv.user_email, amount, cur,  allsend.lastname, allv.busname)
                bal = Wallet.objects.values('ngn').get(user_email=allv.user_email)['ngn']
                ## Notify Vendor ###
                semail.Rec_Money(allv.user_email, amount, cur, allv.busname, bal, allsend.lastname)
                return render(request, 'pay/resp.html', {'status': status,
                                                 'callback': sucess_url,
                                                 'ref': ref,
                                                 'amount': amount})
                    

                   
        else:
            status = 400
            msg = "Invalid Credentials"
            return render(request, 'pay/resp.html', {'status': status,
                                                 'callback': fail_url,
                                                 'msg': msg})
    else:
        return render(request, 'pay.html')


def error(request):
    return render(request, 'errors.html')


def success(request):
    return render(request, 'success.html')


def logout(request):
    s_logout(request)
    return redirect('success')
