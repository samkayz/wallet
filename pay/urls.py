from django.urls import path
from . import views


urlpatterns = [
    path('v1', views.pay, name='pay'),
    path('initiate', views.initiate, name='initiate'),
    path('logout', views.logout, name='logout'),
]
