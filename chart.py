from fusioncharts import FusionCharts
from django.http import HttpResponse
from collections import OrderedDict
from django.core import serializers
from django.http import JsonResponse
from SendEmail import *
from function import *
from web.models import *
from django.db.models import Sum
from django.db.models import Q
My_Class = Main()




class sChart:
    

    def AllUserChart(self):
        resp = My_Class.All_User(role='user')
        count_user = resp[2].count()
        count_vendor = My_Class.All_User(role='vendor')[2].count()
        count_admin = My_Class.All_User(role='admin')[2].count()
        total_trans = My_Class.All_History().count()
        # Chart data is passed to the `dataSource` parameter, like a dictionary in the form of key-value pairs.
        dataSource = OrderedDict()

        # The `chartConfig` dict contains key-value pairs of data for chart attribute
        chartConfig = OrderedDict()
        chartConfig["caption"] = "System Managements"
        chartConfig["subCaption"] = "Total User of the System"
        chartConfig["xAxisName"] = "Country"
        chartConfig["yAxisName"] = "Reserves (MMbbl)"
        chartConfig["numberSuffix"] = ""
        chartConfig["theme"] = "fusion"

        dataSource["chart"] = chartConfig
        dataSource["data"] = []

        dataSource["data"].append({"label": 'Merchant', "value": f'{count_vendor}'})
        dataSource["data"].append({"label": 'User', "value": f'{count_user}'})
        dataSource["data"].append({"label": 'Administrator', "value": f'{count_admin}'})
        column2D = FusionCharts("pie3d", "myFirstChart", "855", "630", "myFirstchart-container", "json", dataSource)
        return column2D

    
    def CurChart(self):
        ngntotal = Transaction_Log.objects.filter(Q(scur='ngn')).aggregate(Sum('amt_snd'))['amt_snd__sum']
        btctotal = Transaction_Log.objects.filter(Q(scur='btc')).aggregate(Sum('amt_snd'))['amt_snd__sum']
        ethtotal = Transaction_Log.objects.filter(Q(scur='eth')).aggregate(Sum('amt_snd'))['amt_snd__sum']
        dataSource = OrderedDict()
        chartConfig = OrderedDict()
        chartConfig["caption"] = "Outward Transactions"
        chartConfig["subCaption"] = ""
        chartConfig["xAxisName"] = "Currency"
        chartConfig["yAxisName"] = "Amount"
        chartConfig["numberSuffix"] = ""
        chartConfig["theme"] = "fusion"

        dataSource["chart"] = chartConfig
        dataSource["data"] = []

        dataSource["data"].append({"label": 'NGN', "value": f'{ngntotal}'})
        dataSource["data"].append({"label": 'BTC', "value": f'{btctotal}'})
        dataSource["data"].append({"label": 'ETH', "value": f'{ethtotal}'})
        trans = FusionCharts("column3d", "transaction", "350", "300", "trans-container", "json", dataSource)
        return trans

    def InwardChart(self):
        ngntotal = Transaction_Log.objects.filter(Q(scur='ngn')).aggregate(Sum('amt_rec'))['amt_rec__sum']
        btctotal = Transaction_Log.objects.filter(Q(scur='btc')).aggregate(Sum('amt_rec'))['amt_rec__sum']
        ethtotal = Transaction_Log.objects.filter(Q(scur='eth')).aggregate(Sum('amt_rec'))['amt_rec__sum']
        dataSource = OrderedDict()
        chartConfig = OrderedDict()
        chartConfig["caption"] = "Inward Transactions"
        chartConfig["subCaption"] = ""
        chartConfig["xAxisName"] = "Currency"
        chartConfig["yAxisName"] = "Amount"
        chartConfig["numberSuffix"] = ""
        chartConfig["theme"] = "fusion"

        dataSource["chart"] = chartConfig
        dataSource["data"] = []

        dataSource["data"].append({"label": 'NGN', "value": f'{ngntotal}'})
        dataSource["data"].append({"label": 'BTC', "value": f'{btctotal}'})
        dataSource["data"].append({"label": 'ETH', "value": f'{ethtotal}'})
        itrans = FusionCharts("column3d", "transactions", "350", "200", "itrans-container", "json", dataSource)
        return itrans